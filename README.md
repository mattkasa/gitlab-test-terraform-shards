## GitLab Test Terraform Shards

This project demonstrates using the generated child pipeline feature to
implement a sharded Terraform repository.

The top level [`.gitlab-ci.yml`](.gitlab-ci.yml) runs
[`generate-pipelines`](generate-pipelines) which generates a child pipeline
consisting of the contents of
[`.gitlab/child-pipeline.gitlab-ci.yml`](.gitlab/child-pipeline.gitlab-ci.yml)
and one templated copy of
[`.gitlab/child-job.gitlab-ci.yml`](.gitlab/child-job.gitlab-ci.yml) for each
directory under the root which contains `*.tf` files.

The goal is for each directory to run Terraform with it's own state file in the
GitLab Terraform HTTP backend, and to only run when changes have been made.

For large projects, this can speed up pipelines significantly when you know only
small, isolated sections of your infrastructure code are going to be changed by
a single MR or commit.
